(* $Id: cryptsystem_64.ml,v 1.2 2001/03/10 16:43:21 gerd Exp $
 * ----------------------------------------------------------------------
 * This module is part of the cryptgps package by Gerd Stolpmann.
 *)

(* The module type of a cryptsystem using 64 bit block ciphers. *)

module type T =
  sig
    
    type key
      (* This is the internal, often preprocessed representation of keys. *)

    val encrypt_ecb : 
	key -> (int * int * int * int) -> (int * int * int * int)
      (* This is the ECB mode of the encryption function. The four ints
       * are numbers from 0 to 65535, and given from MSB to LSB.
       *)

    val encrypt_ecb_int32 : 
	key -> int32 -> int32 -> int32 ref -> int32 ref -> unit
      (* The same as encrypt_ecb, but with an int32 interface *)

    val decrypt_ecb : 
	key -> (int * int * int * int) -> (int * int * int * int)
      (* This is the ECB mode of the decryption function. The four ints
       * are numbers from 0 to 65535, and given from MSB to LSB.
       *)

    val decrypt_ecb_int32 : 
        key -> int32 -> int32 -> int32 ref -> int32 ref -> unit
      (* The same as decrypt_ecb, but with an int32 interface *)

    val prepare : string -> key
      (* Prepares the string representation of a key. *)

    val textkey : key -> string
      (* Gets the string representation back *)

    val is_weak : key -> bool
      (* Determines whether the key is known as being weak. Do not use
       * such keys.
       *)
  end
;;
                             


(* ======================================================================
 * History:
 * 
 * $Log: cryptsystem_64.ml,v $
 * Revision 1.2  2001/03/10 16:43:21  gerd
 * 	int32 experiments
 *
 * Revision 1.1  1999/06/04 20:42:01  gerd
 * 	Initial revision.
 *
 * 
 *)
