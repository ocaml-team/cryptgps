#include <stdio.h>
#include "des.h"


void print4(unsigned char *p) {
    int x3,x2,x1,x0;
    x3 = p[0] << 8 | p[1];
    x2 = p[2] << 8 | p[3];
    x1 = p[4] << 8 | p[5];
    x0 = p[6] << 8 | p[7];

    printf(" %d %d %d %d", x3, x2, x1, x0);
}




main () {
    des_key_schedule k;
    unsigned char out8[8];
    unsigned char out16[16];
    unsigned char ivec[8];
    int i, num;

    des_set_key("abcdefgh",k);


    /* ECB TEST */

    des_ecb_encrypt("12345678", out8, k, 1);

    printf("Result ECB:");
    print4(out8);
    printf("\n");


    /* CBC TEST */
    
    for (i=0; i<8; i++) ivec[i] = 0;
    des_cbc_encrypt("12345678abcdefgh", out16, 16, k, ivec, 1);

    printf("Result CBC:");
    print4(out16);
    print4(out16+8);
    printf("\n");

    /* CFB-64 TEST */

    for (i=0; i<8; i++) ivec[i] = 0;
    num = 0;
    des_cfb64_encrypt("12345678abcdefgh", out16, 16, k, ivec, &num, 1);

    printf("Result CFB-64:");
    print4(out16);
    print4(out16+8);
    printf("\n");

    /* OFB TEST */

    for (i=0; i<8; i++) ivec[i] = 0;
    num = 0;
    des_ofb64_encrypt("12345678abcdefgh", out16, 16, k, ivec, &num);

    printf("Result OFB:");
    print4(out16);
    print4(out16+8);
    printf("\n");
}
